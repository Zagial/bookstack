import os
from flask import Flask, send_file

app = Flask(__name__)
app.debug = False
app.server_name = "esgipxe:5000"

CA_CERTIFICATE_FILE = "/etc/pki/tls/private/esgi-ca.pem"
CA_KEY_FILE = "/etc/pki/tls/private/esgi-ca.key"
EXT_FILE = "default.ext"
EXT_CONTENT = \
"""
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = {domain}
"""

@app.route("/<domain>", methods=['GET'])
def get_certificate(domain):
	ret = ""

	domain_key_file = "{}.key".format(domain)
	csr_file = "{}.csr".format(domain)
	domain_certificate_file = "{}.crt".format(domain)

	with open(EXT_FILE, 'w') as file:
		file.write(EXT_CONTENT.format(domain=domain))

	os.system("openssl genrsa -out {file_name} 2048".format(file_name=domain_key_file))
	
	os.system("openssl req -new -key {key_file} -out {csr_file} \
		-subj \"/C={country}/ST={state}/L={city}/O={organisation}/CN={common_name}\""
		.format(key_file=domain_key_file,
			csr_file=csr_file,
			country="FR",
			state="Paris",
			city="Paris",
			common_name=domain,
			organisation=domain.split('.')[-2:][0]))
	os.system("openssl x509 -req -in {csr_file} -CA {ca_certificate} \
		-CAkey {ca_key} -CAcreateserial -out {generated_certificate} \
		-days 365 -sha256 -passin 'pass:koolix' -extfile {ext_file}"
		.format(csr_file=csr_file,
			ca_certificate=CA_CERTIFICATE_FILE,
			ca_key=CA_KEY_FILE,
			generated_certificate=domain_certificate_file,
			ext_file=EXT_FILE))

	os.system("tar -cf /tmp/{domain}.tar {crt_file} {key_file}".format(domain=domain, crt_file=domain_certificate_file, key_file=domain_key_file))

	for file in [domain_key_file, csr_file, domain_certificate_file]:
		os.remove(file)

	return send_file("/tmp/{domain}.tar".format(domain=domain), as_attachment=False)
