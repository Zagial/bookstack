#!/bin/bash

echo "[+] Generating CA key"
openssl genrsa -aes256 -out /etc/pki/tls/private/esgi-ca.key \
    -passout 'pass:koolix' 2048

echo "[+] Generating CA certificate"
openssl req -x509 -new -nodes -key /etc/pki/tls/private/esgi-ca.key \
    -sha256 -days 365 -out /etc/pki/tls/private/esgi-ca.pem \
    -subj /C=FR/ST=Paris/L=Paris/O=esgi/CN=wiki.esgi.local \
    -passin 'pass:koolix'

echo "[+] Installing certificate-generator.service in /etc/systemd/system"
cp certificate-generator.service /etc/systemd/system/

echo "[+] Reload systemd services"
systemctl daemon-reload

echo "[+] Starting and enabling certificate-generator"
systemctl start certificate-generator
systemctl enable certificate-generator
